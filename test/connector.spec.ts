import {NgConnector} from "../src/ng.connector";
import {ErrorResponse, MatchingRequest} from "../src/models/connector.s";

describe("FakeHttpResponseTest", () => {

    const c: NgConnector = new NgConnector();

    c.configure().setUriOnce("server");
    c.configure().setPrefixOnce("/prefix");

    c.getConnector().useTestingSetup();

    const tbe = c.getConnector().useTesting();

    it("Should make simple GET request",  () => {

        c.getConnector().get("/a").subscribe(result => {

            expect(result !== null);

        }, (error: ErrorResponse) => {

            expect(error !== null);

        });

        const getRequest: MatchingRequest = {method: "GET", url: "server/prefix/a"};
        const req = tbe.expectOne(getRequest);

        req.flush({id:1});
        //req.error({id:1});

    });

    it("Should make simple POST request",  () => {

        c.getConnector().post("/a", {id:1}).subscribe(result => {

            expect(result !== null);

        }, (error: ErrorResponse) => {

            expect(error !== null);

        });

        const getRequest: MatchingRequest = {method: "POST", url: "server/prefix/a"};
        const req = tbe.expectOne(getRequest);

        req.flush({id:1});
        //req.error({id:1});

    });

    it("Should make simple PUT request",  () => {

        c.getConnector().put("/a", {id:1}).subscribe(result => {

            expect(result !== null);

        }, (error: ErrorResponse) => {

            expect(error !== null);

        });

        const getRequest: MatchingRequest = {method: "PUT", url: "server/prefix/a"};
        const req = tbe.expectOne(getRequest);

        req.flush({id:1});
        //req.error({id:1});

    });

    it("Should make simple DELETE request",  () => {

        c.getConnector().delete("/a").subscribe(result => {

            expect(result !== null);

        }, (error: ErrorResponse) => {

            expect(error !== null);

        });

        const getRequest: MatchingRequest = {method: "DELETE", url: "server/prefix/a"};
        const req = tbe.expectOne(getRequest);

        req.flush({id:1});
        //req.error({id:1});

    });

});