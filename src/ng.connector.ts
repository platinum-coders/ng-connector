import {Connector} from './models/connector.s';
import {Authentication} from './models/authentication.s';
import {Configurator} from "./models/configurator.s";

export class NgConnector {

    private connector: Connector = null;

    private authenticator: Authentication = null;

    constructor() {

        this.connector = new Connector();

        this.authenticator = new Authentication(this.connector);

    }

    public configure() : Configurator {

        return this.connector.configure();

    }

    public getConnector() : Connector {

        return this.connector;

    }

    public getAuthenticator() : Authentication {

        return this.authenticator;

    }

}