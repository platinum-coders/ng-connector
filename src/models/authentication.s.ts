import {ConnectionAuthorizationType, Connector} from './connector.s';
import 'rxjs/add/operator/toPromise';
import {Exception} from 'ts-exceptions';

export class Authentication {

	private state: AuthorizationState = AuthorizationState.NotAuthorized;

	private _conn: Connector = null;

	private _currentToken: AuthorizationTokenInterface = null;

	constructor(conn: Connector) {

		this._conn = conn;

	}

	public reset() : void {

		this._currentToken = null;

		this._conn.resetAuthorization();

		this.state = AuthorizationState.NotAuthorized;

	}

	public newToken() : AuthorizationTokenInterface {

		this._currentToken = new AuthorizationToken();

		return this._currentToken;

	}

	public setToken(token: AuthorizationToken) : this {

		this._currentToken = token;

		return this;

	}

	public setAuthorization(token: AuthorizationTokenInterface) : this {

		this._currentToken = token;

        this._conn.setAuthorization({
            type:ConnectionAuthorizationType.AuthorizationBearer,
            token: token.token
        });

        return this;

	}

	public getToken() : AuthorizationTokenInterface {

		return this._currentToken;

	}

	/**
	 * Check if current stored token is Authorized
	 * -----------------------------------------------------------------------------------------------
	 *
	 * Check for validity of current token presented in Authentication
	 * 
	 * If no token presented then empty request will be made resulting
	 * in no authentication response
	 *
	 * @returns {Promise<AuthorizationToken>}
	 */
	public isAuthorized() : Promise<AuthorizationTokenInterface> {

		return new Promise((resolve, reject) => {

			if(this._currentToken !== null) {

				this._conn.setAuthorization({
					type:ConnectionAuthorizationType.AuthorizationBearer,
					token: this._currentToken.token
				});

			}

			this._conn.get(this._conn.tokenCheckPath)
				.toPromise()
				.then(response => {

				console.log("Response authorized");
				console.log(response);

				this.state = AuthorizationState.Authorized;

				resolve(this._currentToken);

			}, (error: ErrorResponse) => {

				console.log("Response error authroized");
				console.log(error);

				console.log("Can't authorize response");

				reject(error);

			});

		});

	}

	/**
	 * Will be starting authorization session
	 * -----------------------------------------------------------------------------------------------
	 *
	 * OAuth authorization session is a kind of network session when
	 * Application asking server about some resource it has for it
	 * can be authorized with and server answers with redirect link
	 *
	 */
	public startAuthorization(doAutomatedRedirect = true) : Promise<AuthenticationProcessResponse> {

		if(this.state === AuthorizationState.Authorizing
			|| this.state === AuthorizationState.Authorized)

			return;

		return new Promise((resolve, reject) => {

			this._conn.get(this._conn.authorizationPath)
				.toPromise()
				.then(response => {

					console.log(" >> Start Authorization - RESPONSE REROUTE ");
					console.log(response);

					resolve({
						status: 200,
						url: null,
						error: 0,
						errorMessage: ""
					});

				}, (error: ErrorResponse)  => {

					console.error(" >> Start Authorization - ERROR RESPONSE REROUTE");
					console.error(error);

					console.log("Can't authorize response will be rerouted");

					if(doAutomatedRedirect) {

						if(typeof error.url !== "undefined" && error.url !== null)

                        	window.location.href = error.url;

						else if(typeof error.url !== "undefined" && error.url === null && error.status < 100)

							error.statusText = "Authorization server is down. Error: " + error.statusText;

                    }

					resolve({
                        status: error.status,
                        url: error.url,
                        error: error.status < 100 ? 500 : error.status,
                        errorMessage: error.statusText
					});

				});

		});

	}

	/**
	 * Finalize Authorization session
	 * -----------------------------------------------------------------------------------------------
	 *
	 * This method requires a code which will be used to finish authorization
	 * session with OAuth server and will result in Authorization Token saved
	 * inside this module
	 *
	 * @param {string} code
	 * @returns {Promise<AuthorizationToken>}
	 */
	public finishAuthorization(code: string) : Promise<AuthorizationTokenInterface> {

		this.state = AuthorizationState.Authorizing;

		return new Promise((resolve, reject) => {

			this._conn.post(
				this._conn.tokenExchangePath,
				{
					grant_type: "authorization_code",
					code: code
				},
				{},
				{useAuthentication: false})
				.toPromise().then((response: OauthTokenInterface) => {

				console.log("TOKEN RESPONSE: ");
				console.log(response);

				this.state = AuthorizationState.Authorized;

				this._currentToken = new AuthorizationToken().fromObject(response);

				this._conn.setAuthorization({
					type:ConnectionAuthorizationType.AuthorizationBearer,
					token: this._currentToken.token
				});

				resolve(this._currentToken);

			}, (error: ErrorResponse)  => {

				console.log("ERROR RESPONSE: ");
				console.log(error);

				this.state = AuthorizationState.Authorized;

				reject(error);

			});

		});

	}

	/**
	 * Use stored Refresh token to get new Access Token
	 * -----------------------------------------------------------------------------------------------
	 *
	 * Refresh authorized state of some token by it's refresh token
	 * OAuth server will issue a new token which will be stored
	 * inside Authenticator
	 *
	 * @returns {Promise<AuthorizationToken>}
	 */
	public refreshAuthorization() : Promise<AuthorizationTokenInterface> {

		return new Promise((resolve, reject) => {

            if(this._currentToken === null)

            	return reject(new Exception("There is no refresh token available for Refresh Authorization. " +
                    "Please be sure to be authorized prior to start token refresh operation", 400));

			this._conn.post(
				this._conn.tokenExchangePath,
				{
					grant_type: OAuthTokenGrantTypes.RefreshToken,
					refresh_token: this._currentToken.refreshToken
				},
				{},
				{useAuthentication: false})
				.toPromise().then((response: OauthTokenInterface) => {

				console.log("TOKEN RESPONSE: ");
				console.log(response);

				this.state = AuthorizationState.Authorized;

				this._currentToken = new AuthorizationToken().fromObject(response);

				this._conn.setAuthorization({
					type:ConnectionAuthorizationType.AuthorizationBearer,
					token: this._currentToken.token
				});

				resolve(this._currentToken);

			}, (error: ErrorResponse)  => {

				console.log("ERROR RESPONSE: ");
				console.log(error);

				this.state = AuthorizationState.Authorized;

				reject(error);

			});

		});

	}

}

/**
 *  Authorization token representation. That representation will be used
 *  within Authorization Service to keep current status of userapi
 */
export class AuthorizationToken implements AuthorizationTokenInterface {

	private _id: string = null;

	private _user: string = null;

	private _expires: number = 0;

	private _refreshId: string = null;

	private _expiresAt: Date = null;

	constructor(str?: string) {

		if(str) {
			this.fromString(str);
		}

	}

	public isExpired() {

		return new Date() > this._expiresAt;

	}

	get token(): string {

		return this._id;

	}

	get user(): string {

		return this._user;

	}

	get refreshToken(): string {

		return this._refreshId;

	}

	get expires(): number {

		return this._expires;

	}

	get expiresAt(): Date {

		return this._expiresAt;

	}

	public fromObject(p: Object) : this {

		if(p.hasOwnProperty("access_token")) {
			this._id = p["access_token"];
		}
		if(p.hasOwnProperty("refresh_token")) {
			this._refreshId = p["refresh_token"];
		}
		if(p.hasOwnProperty("user_id")) {
			this._user = p["user_id"];
		}
		if(p.hasOwnProperty("expires_in")) {
			this._expires = p["expires_in"];
		}

		if(p.hasOwnProperty("expires_at"))
			this._expiresAt = new Date(p["expires_at"]);
		else
			this._expiresAt = new Date(new Date().getTime() + this._expires * 1000);


		return this;

	}

	public fromString(str: string) : this {

		try {

			const p = JSON.parse(str);

			if(p.hasOwnProperty("access_token")) {
				this._id = p.access_token;
			}
			if(p.hasOwnProperty("refresh_token")) {
				this._refreshId = p.refresh_token;
			}
			if(p.hasOwnProperty("user_id")) {
				this._user = p.user_id;
			}
			if(p.hasOwnProperty("expires_in")) {
				this._expires = p.expires_in;
			}

			if(p.hasOwnProperty("expires_at"))
				this._expiresAt = new Date(p["expires_at"]);
			else
				this._expiresAt = new Date(new Date().getTime() + this._expires * 1000);

		} catch (e) {

		}

		return this;
	}

	public toString() : string {

		return JSON.stringify(
			{
				access_token: this._id,
				refresh_token: this._refreshId,
				user_id: this._user,
				expires_in: this._expires,
				expires_at: this._expiresAt
			});

	}

}

export enum AuthorizationState {
	NotAuthorized,
	Authorizing,
	Authorized
}

export enum OAuthTokenGrantTypes {
	AuthorizationCode = "authorization_code",
	RefreshToken = "refresh_token",
	ClientCredentials = "client_credentials"
}

export interface AuthenticationProcessResponse {
	status: number,
	url: string,
	error: number,
	errorMessage: string
}

export interface ErrorResponse {
	headers: any;
	ok: boolean;
	status: number;
	statusText: string;
	type:any;
	url: string;
	_body?: string;
}

export interface OauthTokenInterface {
	access_token: string;
	token_type: string;
	expires_in: number;
	user_id: string;
	refresh_token: string;
}

export interface AuthorizationTokenInterface  {
	token: string;
	user: string;
	refreshToken: string;
	expires: number;
	expiresAt: Date;
	isExpired() : boolean;
	fromObject(o: Object) : AuthorizationTokenInterface;
	fromString(s: string) : AuthorizationTokenInterface;
	toString() : string;
}