import {HttpClient, HttpEvent, HttpRequest, HttpXhrBackend} from '@angular/common/http';
import * as HTTPModule from '@angular/common/http';
import * as HTTPTestingModule from "@angular/common/http/testing";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Configurator} from './configurator.s';
import {Exception} from "ts-exceptions";

export class Connector {

	private _client: HttpClient = null;

	private _conf: Configurator = null;

	private authorization: ConnectionAuthorization = null;

	private _testingFramework: TestingConnectorBackend = null;

	private _defaultConnectorOpt: ConnectorSessionOptions = {
		useAuthentication: true
	};

	constructor() {

		this.useProductionSetup();

		this._conf = new Configurator();

	}

	public configure() : Configurator {

		return this._conf;

	}

	public setAuthorization(a: ConnectionAuthorization) : this {

		this.authorization = a;

		return this;

	}

	public getAuthorization() : ConnectionAuthorization {

		return this.authorization;

	}

	public resetAuthorization() : this {

		this.authorization = null;

		return this;

	}

	/*public request(path: string, options?: ConnectorOptions): Observable<Response> {

		const url = this._conf.uri + this._conf.prefix + path;

		return super.request(url, options);

	}*/

	public get<T>(path: string,
				  options?: ConnectorGetOptions,
				  connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

		const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)

			options = this.createOptions();

        if(cOpt.useAuthentication)
            this.authorize(options);

		const url = this.assembleURL(path);

		return this._client.get(url, options) as Observable<T>;

	}

	public post<T>(path: string,
				   body: any,
				   options?: ConnectorOptions,
				   connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

        const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)
			options = this.createOptions();

        if(cOpt.useAuthentication)
            this.authorize(options);

		const url = this.assembleURL(path);

		return this._client.post(url, body, options) as Observable<T>;

	}

	public put<T>(path: string,
				  body: any,
				  options?: ConnectorOptions,
				  connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

        const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)
			options = this.createOptions();

        if(cOpt.useAuthentication)
            this.authorize(options);

        const url = this.assembleURL(path);

		return this._client.put(url, body, options) as Observable<T>;

	}

	public delete<T>(path: string,
					 options?: ConnectorOptions,
					 connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

        const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)
			options = this.createOptions();

        if(cOpt.useAuthentication)
            this.authorize(options);

        const url = this.assembleURL(path);

		return this._client.delete(url, options) as Observable<T>;

	}

	public patch<T>(path: string,
					body: any,
					options?: ConnectorOptions,
					connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

        const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)
			options = this.createOptions();

        if(cOpt.useAuthentication)
            this.authorize(options);

        const url = this.assembleURL(path);

		return this._client.request(url, body, options) as Observable<T>;

	}

	public head<T>(path: string,
				   options?: ConnectorOptions,
				   connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

        const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)
			options = this.createOptions();

        if(cOpt.useAuthentication)
            this.authorize(options);

        const url = this.assembleURL(path);

		return this._client.head(url, options) as Observable<T>;

	}

	public options<T>(path: string,
					  options?: ConnectorOptions,
					  connectorOptions: ConnectorSessionOptions = null) : Observable<T> {

        const cOpt = connectorOptions !== null ? connectorOptions : this._defaultConnectorOpt;

		if(!options)
			options = this.createOptions();

		if(cOpt.useAuthentication)
			this.authorize(options);

        const url = this.assembleURL(path);

		return this._client.options(url, options) as Observable<T>;

	}

	get authorizationPath() : string {

		return this._conf.oauthAuthorizationPath + "?redirect_uri=" + encodeURIComponent(this._conf.oauthRedirect);

	}

	get tokenExchangePath() : string {

		return this._conf.oauthTokenPath;

	}

	get tokenCheckPath() : string {

		return this._conf.oauthCheckPath;

	}

	public useProductionSetup() {

        const BrowserXHR = HTTPModule.ɵe;

        this._client = new HttpClient(new HttpXhrBackend(new BrowserXHR()));

    }

    public useTestingSetup() {

		const TestXHR = HTTPTestingModule.ɵa;

		this._testingFramework = new TestXHR();

        this._client = new HttpClient(this._testingFramework);

    }

    public useTesting() : TestingConnectorBackend {

		if(this._testingFramework === null)

			throw new Exception(
				"Please switch NgConnector to Testing first by executing" +
				" <NgConnector.useTestingSetup()> instance method",
				400);

		return this._testingFramework;

	}

	private assembleURL(to: string) {

		let path = "";

		if(this._conf.uri !== null)

			path += this._conf.uri;

        if(this._conf.prefix !== null)

            path += this._conf.prefix;

        path += to;

        return path;

	}

    private createOptions() : ConnectorOptions {

        return {
            headers: {},
            params: {}
        };

    }

    private authorize(options: ConnectorOptions) {

        if(this.authorization !== null) {

            if(this.authorization.type === ConnectionAuthorizationType.AuthorizationBearer) {

                options.headers["Authorization"] = "Bearer " + this.authorization.token;

            }

        }

    }

}

export enum ConnectorGETResponseTypes {
	"json",
	"arraybuffer"
}

export interface ConnectorOptions {
	headers?: {[header: string]: string | string[]};
	params?: {[param: string]: string | string[];};
	reportProgress?: boolean;
}

export interface ConnectorGetOptions extends ConnectorOptions {
	reportProgress?: boolean;
	responseType?: ConnectorGETResponseTypes | any;
}

export interface ConnectionAuthorization {
	type: ConnectionAuthorizationType;
	token: string;
}

export enum ConnectionAuthorizationType {
	AuthorizationBearer
}

export interface TestingConnectorBackend {
    handle(req: HttpRequest<any>): Observable<HttpEvent<any>>;
    match(match: string | MatchingRequest | ((req: HttpRequest<any>) => boolean)): TestingRequest[];
    expectOne(match: string | MatchingRequest | ((req: HttpRequest<any>) => boolean), description?: string): TestingRequest;
    expectNone(match: string | MatchingRequest | ((req: HttpRequest<any>) => boolean), description?: string): void;
    verify(opts?: {
        ignoreCancelled?: boolean;
    }): void;
}

export interface TestingRequest {
	flush(data: any) : void;
	error(data: any) : void;
}

export interface MatchingRequest {
	url: string,
	method: string
}

export interface ErrorResponse {
	headers: any;
	status: number;
	statusText: string;
	url: string;
	ok: boolean;
	name: string;
	message: string;
	error: string;
}

export interface ConnectorSessionOptions {
	useAuthentication: boolean;
}