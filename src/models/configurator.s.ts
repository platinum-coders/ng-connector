export class Configurator {

	private _uri: string = null;

	private _prefix: string = null;

	private _oauthAuthorizationPath: string = "/oauth/authorize";

	private _oauthTokenPath: string = "/oauth/token";

	private _oauthCheckPath: string = "/oauth/check";

	private _oauthRedirect: string = "http://localhost";

	public setUriOnce(uri: string) : Configurator {

		if(this._uri === null)
			this._uri = uri;
		return this;

	}

	public setPrefixOnce(prefix: string) : Configurator {

		if(this._prefix === null)
			this._prefix = prefix;
		return this;

	}

	public setOauthAuthorizationPathOnce(path: string) : Configurator {

		if(this._oauthAuthorizationPath === "/oauth/authorize")
			this._oauthAuthorizationPath = path;
		return this;

	}

	public setOauthTokenPathOnce(path: string) : Configurator {

		if(this._oauthTokenPath === "/oauth/token")
			this._oauthTokenPath = path;
		return this;

	}

	public setOauthCheckPathOnce(path: string) : Configurator {

		if(this._oauthCheckPath === "/oauth/check")
			this._oauthCheckPath = path;
		return this;

	}

	public setOauthRedirectOnce(path: string) : Configurator {

		if(this._oauthRedirect === "http://localhost")
			this._oauthRedirect = path;
		return this;

	}

	get uri() : string {
		return this._uri;
	}

	get prefix(): string {
		return this._prefix;
	}

	get oauthAuthorizationPath(): string {
		return this._oauthAuthorizationPath;
	}

	get oauthTokenPath(): string {
		return this._oauthTokenPath;
	}

	get oauthCheckPath(): string {
		return this._oauthCheckPath;
	}

	get oauthRedirect(): string {
		return this._oauthRedirect;
	}

}