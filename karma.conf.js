// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: [
        'jasmine',
        "karma-typescript"
    ],
    files: [
        { pattern: "src/**/*.ts" },
        { pattern: "test/**/*.ts" }
    ],
    preprocessors: {
        "**/*.ts": ["karma-typescript"]
    },
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-typescript')
      //require('@angular/cli/plugins/karma')
    ],
    client:{
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    karmaTypescriptConfig: {
        compilerOptions: {
            module: "commonjs"
        },
        include: [
            "src",
            "test"
        ],
        tsconfig: "./tsconfig.json"
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: ['progress', 'kjhtml',"karma-typescript"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  });
};
